//
//  tableViewExt.swift
//  IraqSunNews
//
//  Created by Magid on 9/15/18.
//  Copyright © 2018 Magid. All rights reserved.
//

import Foundation
extension FirstViewController{


    // When generating sorted table data we can easily use our TableSection to make look up simple and easy to read.
    func sortData() {
        data[.iraq] = ArticlesData.filter(      { $0["catid"] == "19" })
        data[.middleEst] = ArticlesData.filter( { $0["catid"] == "21" })
        data[.world] = ArticlesData.filter(     { $0["catid"] == "22" })
        data[.sport] = ArticlesData.filter(     { $0["catid"] == "33" })
        data[.art] = ArticlesData.filter(       { $0["catid"] == "24" })
        data[.tech] = ArticlesData.filter(      { $0["catid"] == "31" })
        data[.health] = ArticlesData.filter(    { $0["catid"] == "30" })
        //print(data[.iraq]!)
    }



}
