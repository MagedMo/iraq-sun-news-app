//
//  SecondViewController.swift
//  IraqSunNews
//
//  Created by Magid on 8/27/18.
//  Copyright © 2018 Magid. All rights reserved.
//

import UIKit

// Cell Id "groupShow"

class SecondViewController: UIViewController,UICollectionViewDataSource,UICollectionViewDelegate {
    
    var CollectionCate = Array<Categorys>()
    
    @IBOutlet weak var CollectionViewCategory: UICollectionView!
    
    

    override func viewDidLoad() {
        super.viewDidLoad()
        ReadFromPList()
        CollectionViewCategory.delegate = self
        CollectionViewCategory.dataSource = self
        
        
    }
    
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return CollectionCate.count
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell:CateCell = CollectionViewCategory.dequeueReusableCell(withReuseIdentifier: "cellCate", for: indexPath) as! CateCell
        cell.CateName.text = CollectionCate[indexPath.row].title
        cell.CateImage.image = UIImage(named: CollectionCate[indexPath.row].image!)
        
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        performSegue(withIdentifier: "groupShow", sender: CollectionCate[indexPath.row])
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if let dest = segue.destination as? SingleCategory
        {
            if let Cate = sender as? Categorys{
            
                dest.SingleCate = Cate
            }
        
        }
        
    }
    
    func ReadFromPList()   {
        
        let path = Bundle.main.path(forResource: "CategoryPlist", ofType: "plist")!
        let url = URL(fileURLWithPath: path)
        let data = try! Data(contentsOf: url)
        let plist=try! PropertyListSerialization.propertyList(from: data, options: .mutableContainers, format: nil)
        let dicArray=plist as! [[String:String]]
        for dic in dicArray {
        CollectionCate.append(Categorys(id: dic["id"]!, title: dic["title"]!, image: dic["Image"]!))
        }
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

