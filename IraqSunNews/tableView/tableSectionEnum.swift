//
//  tableSectionEnum.swift
//  
//
//  Created by Magid on 9/15/18.
//
//

import Foundation

extension FirstViewController{
    
    
    enum TableSection:Int {
        
        case iraq = 0, middleEst, world, sport, art, tech, health, total
    }
    

    
}
