//
//  TableViewCell.swift
//  IraqSunNews
//
//  Created by Magid on 9/17/18.
//  Copyright © 2018 Magid. All rights reserved.
//

import UIKit

class SingleCateCell: UITableViewCell {

    
    @IBOutlet weak var ImageArticle: UIImageView!
    
    @IBOutlet weak var TitleArticle: UILabel!
    
    @IBOutlet weak var DateArticle: UILabel!
    
    
    
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    override var frame: CGRect {
        get {
            return super.frame
        }
        set (newFrame) {
            var frame =  newFrame
            frame.origin.y += 4
            frame.size.height -= 2 * 5
            super.frame = frame
        }
    }
    

    
    

}
