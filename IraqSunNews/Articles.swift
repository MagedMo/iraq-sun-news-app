//
//  Articles.swift
//  IraqSunNews
//
//  Created by Magid on 9/7/18.
//  Copyright © 2018 Magid. All rights reserved.
//

import Foundation
import UIKit

struct Articles {
    var id:String?
    var title:String?
    var author:String?
    var featured:String?
    var introtext:String?
    var fulltext:String?
    var images:String?
    var catid:Int?
    var created:String?
    
    init(id:String,title:String,author:String,featured:String, introtext:String,images:String,fulltext:String, catid:Int,created:String){
    
        self.id         = id
        self.title      = title
        self.author     = author
        self.featured   = featured
        self.introtext  = introtext
        self.catid      = catid
        self.fulltext   = fulltext
        self.images     = images
        self.created    = created
    
    }
    
    init(){}
}
