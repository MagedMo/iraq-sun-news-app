//
//  Api.swift
//  IraqSunNews
//
//  Created by Magid on 9/4/18.
//  Copyright © 2018 Magid. All rights reserved.
//

import Foundation
import SwiftyJSON
import Alamofire

class Api:NSObject{
    
    
    class func GetArticle(completionHandler: @escaping(_ ArticlesData:[[String:String]]?) -> Void)
    {
        
        let url = URL(string: "http://iraq-sunnews.com")
        
        Alamofire.request(url!, method: .get, encoding: URLEncoding.default, headers: nil)
            .responseJSON { response in
                
                let data =  response.value as! [[String:String]]
                //let json = JSON(data!)
                //print (data)
                completionHandler(data)

                }
                //print(ArticlesClass)
    }

    
    // Single Cate
   class func SingleCat(numOfArtcles:Int? = 10 , catid:String, completionHandler: @escaping (_ Articles: [Articles]?) -> Void)
    {
        
        var ArticlesClass = [Articles]()
        let url = URL(string: "http://iraq-sunnews.com/index.php?")
        let Parameters = [
            "task" : "getContents",
            "token": "xxmbwsefkoszuw",
            "catid": catid
        ]
        
        Alamofire.request(url!, method: .get, parameters: Parameters, encoding: URLEncoding.default, headers: nil)
            .responseJSON { response in
                let data =  response.result.value
                let json = JSON(data!)
                //print (json)
                guard let dataArr = json.array else{return}
                
                for data in dataArr
                {
                    
                    var Articlesdata = Articles()
                    
                    Articlesdata.id          = data["id"].string ?? ""
                    Articlesdata.title       = data["title"].string ?? ""
                    Articlesdata.images      = data["images"].string ?? ""
                    Articlesdata.created     = data["created"].string ?? ""
                    ArticlesClass.append(Articlesdata)
                }
                completionHandler(ArticlesClass)
        }

    }
    
    // Single Article
    class func SingleArt(id:String, completionHandler: @escaping (_ Article: [Articles]?) -> Void)
    {
        
        var SingleArticle = [Articles]()
        
        let url = URL(string: "http://iraq-sunnews.com/index.php?")
        let Parameters = [
            "task"  : "getContents",
            "token" : "xmulfrxnmqobymhdmverbjnx",
            "id"    : id
        ]
        
        Alamofire.request(url!, method: .get, parameters: Parameters, encoding: URLEncoding.default, headers: nil)
            .responseJSON { response in
                let data =  response.result.value
                let json = JSON(data!)
                
                
                    var SingleArticleData = Articles()
                    
                    SingleArticleData.id          = json["id"].string ?? ""
                    SingleArticleData.title       = json["title"].string ?? ""
                    SingleArticleData.introtext   = json["introtext"].string ?? ""
                    SingleArticleData.fulltext    = json["fulltext"].string ?? ""
                    SingleArticleData.author      = json["author"].string ?? ""
                    SingleArticleData.images      = json["images"].string ?? ""
                    SingleArticleData.created     = json["created"].string ?? ""
                    SingleArticleData.catid       = json["catid"].int ?? 0
                    SingleArticleData.featured    = json["featured"].string ?? ""
                    SingleArticle.append(SingleArticleData)
                
                completionHandler(SingleArticle)
        }
        
    }
    
    


}

