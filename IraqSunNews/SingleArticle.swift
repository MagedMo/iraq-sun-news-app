//
//  SingleArticle.swift
//  IraqSunNews
//
//  Created by Magid on 9/18/18.
//  Copyright © 2018 Magid. All rights reserved.
//

import UIKit



class SingleArticle: UIViewController {
    
    var SingleArticleFromIndex:[String:String]?
    var SingleArticle:Articles?
    var SingleArtData = [Articles]()
    var id:String?
    
    @IBOutlet weak var ImageArticle: UIImageView!
    @IBOutlet weak var TitleArticle: UILabel!
    @IBOutlet weak var DateArticle: UILabel!
    @IBOutlet weak var AthorArticle: UILabel!
    @IBOutlet weak var ContentArticle: UILabel!
    @IBOutlet weak var fullTextArticle: UILabel!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        lodeArtclesCate()
        //print(SingleArticle?.id)

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func setData() {
        
        for data in SingleArtData
        {
            self.TitleArticle.text      = data.title
            self.AthorArticle.text      = data.author
            let introText = data.introtext?.withoutHtmlTags
            self.ContentArticle.text    = introText
            let fulltext = data.fulltext?.withoutHtmlTags
            self.fullTextArticle.text = fulltext
            self.DateArticle.text       = data.created
           // print(data.images)
            DownloadImage.getImage(ImageDic: data.images!, completion: { image in
                self.ImageArticle.image = image
            })
            
            //SetImage(url:data.images!)
       // print(data.title)
        }
        
    }
    
    func lodeArtclesCate()
    {
        if let id = SingleArticleFromIndex?["id"]{
            self.id = id
        }
        if let id = SingleArticle?.id{
        
            self.id = id
        }
        
        Api.SingleArt(id: (self.id)!) { (Article: [Articles]? )
            in
            
            if let Article = Article{
                
                self.SingleArtData = Article
                
                //print(self.SingleArtData)
                self.setData()

            }
        }
        
        
    }
    func SetImage(url:String){
        //paralel process
        DispatchQueue.global().async {
            
            do{
                // load json server
                let AppURL=URL(string: "http://iraq-sunnews.com/\(url)")!
                let data=try Data(contentsOf: AppURL)
                
                // access to UI
                DispatchQueue.global().sync {
                    self.ImageArticle.image = UIImage(data: data)
                }
                
            }
            catch {
                print("cannot load from server")
            }
            
        }
    }

   
}


extension String {
    var withoutHtmlTags: String {
        return self.replacingOccurrences(of: "<[^>]+>", with: "", options: .regularExpression, range: nil)
    }
}

