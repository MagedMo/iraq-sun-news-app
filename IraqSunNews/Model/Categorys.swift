//
//  Category.swift
//  IraqSunNews
//
//  Created by Magid on 9/6/18.
//  Copyright © 2018 Magid. All rights reserved.
//

import UIKit

struct Categorys {
    
    var id:String?
    var title:String?
    var image:String?
    
    init(id:String, title:String, image:String)
    {
        self.id     = id
        self.title  = title
        self.image  = image
    }
    
    init() {
        
    }
    
    
    
}


