//
//  SingleCategory.swift
//  IraqSunNews
//
//  Created by Magid on 9/17/18.
//  Copyright © 2018 Magid. All rights reserved.
//

import UIKit

// Cell Id "ShowArticleSegue"


class SingleCategory: UIViewController,UITableViewDelegate,UITableViewDataSource {

    
    var SingleCate:Categorys?
    var SinglCateData = [Articles]()
    
    @IBOutlet weak var SingleCateTableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        SingleCateTableView.delegate = self
        SingleCateTableView.dataSource = self
        lodeArtclesCate()
        
        self.title = SingleCate?.title
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return SinglCateData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell:SingleCateCell = SingleCateTableView.dequeueReusableCell(withIdentifier: "SingleCateCell", for: indexPath) as! SingleCateCell
        cell.TitleArticle.text = SinglCateData[indexPath.row].title
        cell.DateArticle.text = SinglCateData[indexPath.row].created
         DownloadImage.getImage(ImageDic: (SinglCateData[indexPath.row].images)!,
                                     completion: { image in
        cell.ImageArticle.image = image
         
         })
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        performSegue(withIdentifier: "ShowArticleSegue", sender: SinglCateData[indexPath.row])

    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if let dest = segue.destination as? SingleArticle
        {
            if let Article = sender as? Articles{
                
                dest.SingleArticle = Article
            }
            
        }

        
    }
    
    
    
    func lodeArtclesCate()
    {
        Api.SingleCat(catid: (SingleCate?.id)!) { (Articles: [Articles]? )
            in
            
            if let Articles = Articles{
                
                self.SinglCateData = Articles
                
                DispatchQueue.main.async {
                    
                    self.SingleCateTableView.reloadData()
                }
                
               // print(self.SinglCateData)
                
            }
    }


    }
}
