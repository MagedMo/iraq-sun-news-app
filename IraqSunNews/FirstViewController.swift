//
//  FirstViewController.swift
//  IraqSunNews
//
//  Created by Magid on 8/27/18.
//  Copyright © 2018 Magid. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON


class FirstViewController: UIViewController, UITableViewDelegate, UITableViewDataSource{
    

    @IBOutlet weak var IraqSunNewsTB: UITableView!
    
        // This is the size of our header sections that we will use later on.
    let SectionHeaderHeight: CGFloat = 40
    
    // Data variable to track our sorted data.
    var data = [TableSection: [[String: Any]]]()
    
    
    var ArticlesData        = [[String:String]]()
    var CategoryData        = [Categorys]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
       self.IraqSunNewsTB.delegate = self
       self.IraqSunNewsTB.dataSource = self

        
        
        getArticles()
        self.title = "جريدة شمس العراق "
    }
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
         return TableSection.total.rawValue
    }
    
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
    // If we wanted to always show a section header regardless of whether or not there were rows in it,
    // then uncomment this line below:
    //return SectionHeaderHeight
    // First check if there is a valid section of table.
    // Then we check that for the section there is more than 1 row.
    if let tableSection = TableSection(rawValue: section), let SectionName = data[tableSection], SectionName.count > 0 {
    return SectionHeaderHeight
        }
         return 0
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
       
        if let tableSection = TableSection(rawValue: section), let Section = data[tableSection] {
            return Section.count

        }
         return 0
    }
    
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let view = UIView(frame: CGRect(x: 0, y: 0, width: tableView.bounds.width, height: SectionHeaderHeight))
        view.backgroundColor = UIColor(red: 7.0/255.0, green: 104.0/255.0, blue: 159.0/255.0, alpha: 1)
        let label = UILabel(frame: CGRect(x: 15, y: 0, width: tableView.bounds.width - 30, height: SectionHeaderHeight))
        label.font = UIFont.boldSystemFont(ofSize: 15)
        label.textColor = UIColor.black
        label.textAlignment = .right
        if let tableSection = TableSection(rawValue: section) {
            switch tableSection {
            case .iraq:
                label.text = "اخبار العراق"
            case .middleEst:
                label.text = "الشرق الاوسط "
            case .world:
                label.text = "اخبار العالم"
            case .sport:
                label.text = "اخبار الرياضة"
            case .art:
                label.text = "اخبار الفن"
            case .tech:
                label.text = "الاخبار التقنية"
            case .health:
                label.text = "اخبار الصحة"
            default:
                label.text = ""
            }
        }
        view.addSubview(label)
        return view
    }
    
    
        
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell:IndexTableViewCell = IraqSunNewsTB.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! IndexTableViewCell
    // Similar to above, first check if there is a valid section of table.
    // Then we check that for the section there is a row.
    if let tableSection = TableSection(rawValue: indexPath.section), let article = data[tableSection]?[indexPath.row] {
        
        cell.CellTitle.text = article["title"] as! String?
        //cell.CellTitle.textAlignment = .right
        cell.CellDate.text = article["created"] as! String?
        cell.CellDate.textAlignment = .left
        let images:String = article["images"] as! String
        
        DownloadImage.getImage(ImageDic: images, completion:{ image in
        
            cell.CellImage.image = image
        })
        
        
        }
    return cell
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let tableSection = TableSection(rawValue: indexPath.section), let article = data[tableSection]?[indexPath.row]{
        performSegue(withIdentifier: "ShowArticleSegue1", sender: article)
        //print(ArticlesData[indexPath.row])
        }
    
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if let dest = segue.destination as? SingleArticle
        {
            if let Article = sender as? [String:String]{
                
                dest.SingleArticleFromIndex = Article
            }
            
        }
        
        
    }

    
    

}
