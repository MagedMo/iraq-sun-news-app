//
//  ConvertToDic.swift
//  IraqSunNews
//
//  Created by Magid on 9/29/18.
//  Copyright © 2018 Magid. All rights reserved.
//

import Foundation
import UIKit

class ConvertToDic
{
    class func convertToDictionary(text: String) -> [String: Any]? {
        if let data = text.data(using: .utf8) {
            do {
                return try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any]
            } catch {
                print(error.localizedDescription)
            }
        }
        return nil
    }
    
    
}




