//
//  DownloadImagesFromURL.swift
//  IraqSunNews
//
//  Created by Magid on 9/26/18.
//  Copyright © 2018 Magid. All rights reserved.
//

import Foundation
import UIKit
import SwiftyJSON

class DownloadImage
{
    static let cache = NSCache<NSString, UIImage>()
    
    class func downloadImage(ImageDic:String, completion: @escaping(_ Image:UIImage?) -> () )
    {
        let imageDic = JSON(ConvertToDic.convertToDictionary(text: ImageDic)!)
        let url = String(describing: imageDic["image_fulltext"])
        var DImage:UIImage?

       
            
            do{
                // load json server
                let ImageURL=URL(string: "http://iraq-sunnews.com/\(url)")!
                let data=try Data(contentsOf: ImageURL)
                
                // access to UI
                DispatchQueue.main.async {
                DImage = UIImage(data: data)
                    completion(DImage)
                }
                if DImage != nil {
                    cache.setObject(DImage!, forKey: ImageDic as NSString)
                }
            }
            catch {
                print("cannot load from server")
            }
       
    }
    static func getImage(ImageDic:String, completion: @escaping(_ Image:UIImage?)->()) {
        if let image = cache.object(forKey: ImageDic as NSString) {
            completion(image)
        } else {
            downloadImage(ImageDic: ImageDic, completion: completion)
        }
    }
    
}
