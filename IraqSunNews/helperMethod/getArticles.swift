//
//  getArticles.swift
//  IraqSunNews
//
//  Created by Magid on 9/8/18.
//  Copyright © 2018 Magid. All rights reserved.
//

import Foundation
import UIKit
import SwiftyJSON

extension FirstViewController{

    func  getArticles()
    {
        Api.GetArticle {
            (ArticlesData: [[String:String]]?)
            in
            
            if let ArticlesData = ArticlesData{
                
                self.ArticlesData = ArticlesData
                //print(self.ArticlesData)
            }
            self.sortData()
            DispatchQueue.main.async {
                self.IraqSunNewsTB.reloadData()
                
            }
        }
        
    }
    
}
    


/*
 Api.GetArticle(catid: Catid!)
 {(Articles: [Articles]? ) in
 
 guard let Articles = Articles else{return}
 
 self.ArticlesData = Articles
 
 //print(self.ArticlesData)
 }*/

