//
//  IndexTableViewCell.swift
//  IraqSunNews
//
//  Created by Magid on 9/26/18.
//  Copyright © 2018 Magid. All rights reserved.
//

import UIKit

class IndexTableViewCell: UITableViewCell {

    @IBOutlet weak var CellImage: UIImageView!
    
    @IBOutlet weak var CellTitle: UILabel!
    
    @IBOutlet weak var CellDate: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    
    override var frame: CGRect {
        get {
            return super.frame
        }
        set (newFrame) {
            var frame =  newFrame
            frame.origin.y += 4
            frame.size.height -= 2 * 5
            super.frame = frame
        }
    }

}
